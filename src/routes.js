import { BrowserRouter, Route } from 'react-router-dom'
import React, { Component } from 'react';
import Dashboard from './views/Dashboard/dashboard';
import Product from './views/Poduct/product'

class Routes extends Component {

    render() {
        return (
            <BrowserRouter>
                <div>
                    <Route exact path="/" component={Dashboard} />
                    <Route exact path="/product/:id" component={Product} />
                </div>
            </BrowserRouter>
        )
    }
}

export default Routes;

