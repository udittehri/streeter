import React, { Component } from 'react';
// import axios from 'axios';


class HeaderComponent extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a  href= '#' className="navbar-brand" >My Awesome Shop</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse " id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item active">
                  <a  href= '#' className="nav-link" >Home <span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item">
                  <a  href= '#' className="nav-link" >Features</a>
                </li>
                <li className="nav-item">
                  <a  href= '#' className="nav-link" >Pricing</a>
                </li>
              </ul>
            </div>
          </nav>
        );
    }
}

export default HeaderComponent;



