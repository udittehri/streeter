import React, { Component } from 'react';
import DashboardComponent from './../../components/dashboardComponent'
import HeaderComponent from '../../Common/header';
import DashboadServies from '.././../services/dashboardServices'

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            pageNumber: 1
        };
    }
    componentDidMount = async () => {
        this.getAllProducts();

    }
    getAllProducts = async () => {
        let r = await DashboadServies.getAllProducts(this.state.pageNumber);
        console.log(r, " This is response ");
        if (r.data.products) {
            let p = r.data.products
            let c = this.state.products
            c = c.concat(p)
            debugger
            this.setState({
                products: c,
                pageNumber: this.state.pageNumber++
            })
        }
    }
    render() {
        return (
            <div>
                <HeaderComponent />
                <DashboardComponent
                    products={this.state.products}
                    getMore={() => this.getAllProducts()} />
            </div>
        );
    }
}
export default Dashboard;
