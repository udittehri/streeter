import React, { Component } from 'react';
import ProductComponent from './../../components/productComponent'
import HeaderComponent from '../../Common/header';
import ProductServices from '../../services/productServices';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product : {}
        };
    }
    // componentWillReceiveProps = async ()=>{
    //     console.log(this.props.match.params.id,'------------------');
        
    //     // let r = await ProductServices.getProducts(this.props.match)
    // }  

    // I AM NOT USING 'getDerivedStateFromProps' BECAUSE THIS LEADS TO BUGS AS STATED IN THE DOCUMENT 

    componentDidMount = async () => {
        console.log(this.props,'-----');
        
        let r = await ProductServices.getProducts(this.props.match.params.id)
        console.log(r, " This is response ");
        if (r)
        this.setState({product : r.data}) 
    }
    render() {
        return (
            <div>
                <HeaderComponent />
                <ProductComponent 
                product = {this.state.product} />
            </div>
        );
    }
}
export default Product;
