import axios from 'axios';

const ProductServices = {
    getProducts : async (id) => {
        const response = await axios.get(`https://assignment-appstreet.herokuapp.com/api/v1/products/${id}`)
        return response;
    } 
}

export default ProductServices;