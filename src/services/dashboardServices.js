import axios from 'axios';

const dashboardServices = {
    getAllProducts : async (pageNumber = 1) => {
        // https://assignment-appstreet.herokuapp.com/api/v1/products?page=1
        const response = await axios.get(`https://assignment-appstreet.herokuapp.com/api/v1/products?page=${pageNumber}`)
        return response;
    } 
}

export default dashboardServices;