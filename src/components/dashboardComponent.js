import React, { Component } from 'react';
// import axios from 'axios';
import { Link } from 'react-router-dom'

class DashboardComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="container mt-4">
                {/* {this.props.products} */}
                {this.props.products &&
                    <div className="col">
                        <div className="row">
                            {this.props.products.map((edx,key = edx._id) =>
                                <div className="col-12 col-md-6 col-lg-3 mb-3 ">
                                    {/* <div className="card-deck"> */}
                                    <Link to={`/product/${edx._id}`} params={{ productId: edx._id }} ><div className="card" >
                                        <img src={edx.images[0] || ''} className="card-img-top size" alt="..." width="200" height="200" />
                                        <div className="card-body">
                                            <h5 className="card-title"> {edx.name} </h5>
                                            <p className="card-text">&#x20b9; { new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format( edx.sale_price)  } </p>
                                        </div>
                                    </div> </Link>
                                </div>)}
                        </div>
                    </div>}
                    <p className="bg-light text-center" onClick={()=>this.props.getMore()}> Load More</p>
            </section>
        );
    }
}

export default DashboardComponent;
