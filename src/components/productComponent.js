import React, { Component } from 'react';
// import axios from 'axios';

class ProductComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            primary: '',
            units: 0,
            selectedAtr: [],
            selectedOptions: []
        }
    }
    componentDidMount() {
        console.log(this.props.product.primary_product, '239h2ifdn2938h');
        this.setState({
            currentImage: this.props.product.primary_product ? this.props.product.primary_product.images[0] : '',
            // primary:  this.props.product.primary_product? this.props.product.primary_product :''
        })

        // primary: props.product.primary_product

    }
    changeUnits(isInc) {
        if (isInc) {
            this.setState({ units: this.state.units + 1 })
        }
        else if (!isInc && this.state.units > 0)
            this.setState({ units: this.state.units - 1 })

    }
    changeProducts = (option, optionId) => {
        let initialProp = { ...this.props }
        if (this.state.selectedOptions.length == 0)
            this.state.selectedOptions.push(option)

        else {
            // debugger
            //If it is confirmed we have only two attributes.
            let foundSame = false;
            this.state.selectedOptions.map((hdx, index) => {
                if (hdx.attrib_id == option.attrib_id) {
                    this.state.selectedOptions[index] = option;
                    foundSame = true;
                }
            })
            if (!foundSame) this.state.selectedOptions.push(option)

            if (this.state.selectedOptions.length == 2) {
                this.props.product.product_variations.map((edx) => {
                    if (edx.sign.includes(this.state.selectedOptions[0]._id) && edx.sign.includes(this.state.selectedOptions[1]._id)) {
                        let des = this.state.primary.desc;
                        edx.des = des;
                        this.setState({ primary: edx })
                    }
                })
            }

        }
    }

    componentWillReceiveProps() {
        console.log(this.props.product.primary_product, '239h2ifdn2938h');

    }
    selectedClass (option){
        debugger
        if (this.state.selectedOptions[0] && (this.state.selectedOptions[0]._id == option._id)  ) return 'abc';
        else if (this.state.selectedOptions[1] && (this.state.selectedOptions[1]._id == option._id)  ) return 'abc';
        else return "text-info bg-light p-2 m-1 "

        // "text-info bg-light p-2 m-1 "

    }
    static getDerivedStateFromProps(props, state) {
        console.log(77);
        if (!state.primary)
            return {
                primary: props.product.primary_product
            }

    }
    render() {
        return (
            <section className="container mt-4">
                {/* Hey Hey  */}
                <div>
                    <div class="card">
                        <div class="container-fliud">
                            <div class="wrapper row">
                                <div class="preview col-md-6">

                                    <div class="preview-pic tab-content">
                                        <div class="" id="pic-1"><img src={this.state.currentImage ? this.state.currentImage : this.state.primary ? this.state.primary.images[0] : 'lolo'} /></div>
                                    </div>
                                    <ul class="preview-thumbnail nav nav-tabs">
                                        {this.state.primary && this.state.primary.images.map((idx) =>
                                            <li onClick={() => this.setState({ currentImage: idx })}><a ><img src={idx} /></a></li>
                                        )}
                                    </ul>

                                </div>
                                <div class="details col-md-6">
                                    <h3 class="product-title"> {this.state.primary ? this.state.primary.name : ''} </h3>

                                    <p class="product-description">{this.state.primary ? this.state.primary.desc : ''}</p>
                                    <hr />

                                    <h3 className="font-weight-bold" > &#x20b9; {this.state.primary ? new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(this.state.primary.sale_price) : ''}
                                    </h3>
                                    <strike> <span className="font-weight-light text-sm"> &#x20b9;{this.state.primary ? new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(this.state.primary.mark_price) : ''} </span> </strike>
                                    {/* <p> You Save : {this.state.primary.sale_price - this.state.primary.mark_price} </p> */}
                                    <hr />

                                    {this.props.product.attributes && this.props.product.attributes.map((edx) =>
                                        <>
                                            <h5 className="d-inline p-2"> {edx.name} available
                                            {this.props.product.options && this.props.product.options.map((fdx) =>
                                                <>
                                                    {edx._id === fdx.attrib_id && <span className={ this.selectedClass(fdx)} onClick={() => this.changeProducts(fdx, edx._id)} style={{ display: 'inline' }}>{fdx.name}</span>}
                                                </>
                                            )}</h5>
                                        </>
                                    )}
                                    <div className="units" >
                                        <button class=" btn btn-default text-info bg-light" type="button" onClick={() => this.changeUnits(false)}><span class="fa fa-heart"></span> - </button>
                                        <button class=" btn btn-default text-info bg-light" type="button"><span class="fa fa-heart"></span> {this.state.units} </button>
                                        <button class=" btn btn-default text-info bg-light" type="button" onClick={() => this.changeUnits(true)}><span class="fa fa-heart"></span> + </button>

                                    </div>
                                    <hr />
                                    <div class="action">
                                        <button class="add-to-cart btn btn-default" type="button">add to cart</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ProductComponent;
